const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// [serction] JSON Web Tokens
		// JWT is a way of securely passing information from the server to the frontend or to other parts of server

module.exports.createAccessToken = (user) => {

	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	};

	return jwt.sign(data, secret, {});

};
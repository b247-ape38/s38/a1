const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = (reqBody) => {

	 return User.find({email : reqBody.email}).then(result => {

	 	if(result.length > 0) {
	 		return true;
	 	} else {
	 		return false;
	 	};
	 });
};

module.exports.registerUser = (reqBody) => {

	let newUser = new User({

		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)

	})

	return newUser.save().then((user, error) => {

		if(error) {

			return false;

		} else {

			return true;
		};
	});
};

// login user authenticatioon

module.exports.loginUser = (reqBody) => {

	// We use the "findOne" method instead of the  "find" method which return all records that match  the search criteria
	// The "findOne" method returns the first record in the collection that matches the search criteria
	return User.findOne({email : reqBody.email}).then(result => {

		//User doesn't exist
		if(result == null){
			return false;

		// User exist
		} else {

			// the "compareSync" method is used to compare a non encrypted password fromthe login form to the excrypted password retrived from the database and it returns "true" of "false" value depending on the result
			// A good coding practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form pf is+Noun
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){

				// Generate an access token
				return {access : auth.createAccessToken(result)}
			} else {

				return false;
			}
		}
	})
}



// Activity 

	module.exports.detailUser = (reqBody) => {

	return User.findById(reqBody).then((result, err) => {
		
		if(err){
			
			return false;
		} else {
               
               reqBody.password = ""
               result.password = reqBody.password;
			return result;
		}
	})
}
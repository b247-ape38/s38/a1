const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController");

// check email
router.post("/checkEmail", (req, res) => {

	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// register user
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// route for user authentication
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


// Activity
router.post("/details", (req, res) => {

	userController.detailUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;